/** \file MenuSettings.java. Settings activity */

package com.arnaud.metronome;

import android.app.Activity;
import android.os.Bundle;

import android.content.SharedPreferences;

/*UI*/
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.lang.Exception;
import java.lang.reflect.Field;
import java.util.ArrayList;//il me faut une liste pour le spinner juste pour rajouter generated et muted. On peut pas étendre un tableau java!!! C'est toujours mieux que d'avoir la liste en dur dans string-array strings.xml
import java.util.List;

//import android.util.Log;

/** MenuSettings uses SharedPreferences by default to keep data persistent.
* It can be overridden in order to provide an easy settings activities (for the settings in the bases of MenuStructures)
* The buffer size of AudioTrack is 2*44100.
* So by reducing it, we can lower bpms (<0.2).
* By increasing it, we can feed signal with high freq (>120000).
* Overall, it's pretty much useless and should be kept as-is.
*/

public class MenuSettings extends Activity
{
	final int GENERATED = 0;
	final int MUTED = 1;
	
	protected Spinner spinnerTic;//To be modified in MiniSettings
	protected Spinner spinnerToc;
	private EditText editTextTicFreq;
	private EditText editTextTicLength;
	private EditText editTextTocFreq;
	private EditText editTextTocLength;
	private CheckBox checkboxScreen;
	
	private int spinnerTicPosition;
	private int spinnerTocPosition;
	List<String> listClics = new ArrayList<String>();
	ArrayAdapter<String> adapterClics;

	protected int tic;
	protected int toc;
	//int ticOffset = Integer.parseInt(editTextTicOffset.getText().toString());
	protected float ticFreq;
	protected int ticLength;
	//int tocOffset = Integer.parseInt(editTextTocOffset.getText().toString());
	protected float tocFreq;
	protected int tocLength;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menusettings);
	
		spinnerTic = (Spinner) findViewById(R.id.menuSettingsSpinnerTic);
		spinnerToc = (Spinner) findViewById(R.id.menuSettingsSpinnerToc);
		listClics.add(getResources().getString(R.string.generated));
		listClics.add(getResources().getString(R.string.muted));
		try {
			Field [] fields = R.raw.class.getFields();
			for (int i=0; i<fields.length; i++)
				listClics.add(fields[i].getName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		adapterClics = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listClics);
		adapterClics.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerTic.setAdapter(adapterClics);
		spinnerToc.setAdapter(adapterClics);
		spinnerTic.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
				LinearLayout layoutTic = (LinearLayout) findViewById(R.id.menuSettingsLayoutTicGenerate);
				if (position ==  GENERATED)
					layoutTic.setVisibility(View.VISIBLE);
				else
					layoutTic.setVisibility(View.GONE);
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		spinnerToc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
				LinearLayout layoutToc = (LinearLayout) findViewById(R.id.menuSettingsLayoutTocGenerate);
				if (position == GENERATED)
					layoutToc.setVisibility(View.VISIBLE);
				else
					layoutToc.setVisibility(View.GONE);
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		//editTextTicOffset = (EditText) findViewById(R.id.menuSettingsEditTextTicOffset);
		editTextTicFreq = (EditText) findViewById(R.id.menuSettingsEditTextTicFreq);
		editTextTicLength = (EditText) findViewById(R.id.menuSettingsEditTextTicLength);
		//editTextTocOffset = (EditText) findViewById(R.id.menuSettingsEditTextTocOffset);
		editTextTocFreq = (EditText) findViewById(R.id.menuSettingsEditTextTocFreq);
		editTextTocLength = (EditText) findViewById(R.id.menuSettingsEditTextTocLength);
		checkboxScreen = (CheckBox) findViewById(R.id.menuSettingsCheckBoxScreen);
		
		loadSettings();
	}
	
	public void loadSettings()
	{
		SharedPreferences settings = getSharedPreferences("settings", 0);
		spinnerTicPosition = settings.getInt("tic",0);
		spinnerTocPosition = settings.getInt("toc",0);
		if (spinnerTicPosition >= adapterClics.getCount())
			spinnerTicPosition = 0;
		if (spinnerTocPosition >= adapterClics.getCount())
			spinnerTocPosition = 0;
		spinnerTic.setSelection(spinnerTicPosition);
		spinnerToc.setSelection(spinnerTocPosition);
		//editTextTicOffset.setText(String.valueOf(settings.getInt("ticOffset",0)));
		editTextTicFreq.setText(String.valueOf(settings.getFloat("ticFreq",880)));
		editTextTicLength.setText(String.valueOf(settings.getInt("ticLength",50)));
		//editTextTocOffset.setText(String.valueOf(settings.getInt("tocOffset",0)));
		editTextTocFreq.setText(String.valueOf(settings.getFloat("tocFreq",660)));
		editTextTocLength.setText(String.valueOf(settings.getInt("tocLength",50)));
		checkboxScreen.setChecked(settings.getBoolean("isScreenAlwaysOn",false));
	}

	public void saveSettings()
	{
		SharedPreferences settings = getSharedPreferences("settings", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("tic",tic);
		editor.putInt("toc",toc);
		//editor.putInt("ticOffset",ticOffset);
		editor.putFloat("ticFreq",ticFreq);
		editor.putInt("ticLength",ticLength);
		//editor.putInt("tocOffset",tocOffset);
		editor.putFloat("tocFreq",tocFreq);
		editor.putInt("tocLength",tocLength);
		editor.putBoolean("isScreenAlwaysOn",checkboxScreen.isChecked());
		
		editor.commit();
	}

	public void onMenuSettingsOk (View v)
	{
		if (editTextTicFreq.getText().toString().matches("") || editTextTicLength.getText().toString().matches("") || editTextTocFreq.getText().toString().matches("") || editTextTocLength.getText().toString().matches("")) {
			Toast.makeText(getApplicationContext(),R.string.toastEnterSomeValue,Toast.LENGTH_SHORT).show();
			return;
		}

		tic = spinnerTic.getSelectedItemPosition();
		toc = spinnerToc.getSelectedItemPosition();
		ticFreq = Float.parseFloat(editTextTicFreq.getText().toString());
		tocFreq = Float.parseFloat(editTextTocFreq.getText().toString());
		/* int ticOffset = Integer.parseInt(editTextTicOffset.getText().toString());
		int tocOffset = Integer.parseInt(editTextTocOffset.getText().toString());*/
		try {
			ticLength = Integer.parseInt(editTextTicLength.getText().toString());
			tocLength = Integer.parseInt(editTextTocLength.getText().toString());	
		} catch (NumberFormatException e) {
			Toast.makeText(getApplicationContext(),R.string.toastIntTooBig,Toast.LENGTH_SHORT).show();
			return;
		}

		if (ticFreq < 40 || ticFreq > 44100/2 || tocFreq < 40 || tocFreq > 44100/2) {
			Toast.makeText(getApplicationContext(),R.string.menuSettingsToastSignalFreqWarning,Toast.LENGTH_SHORT).show();
			return;
		}

		saveSettings();
		setResult(1,null);
		finish();
	}
	
	public void onMenuSettingsCancel (View v)
	{
		setResult(0,null);
		finish();
	}
}
