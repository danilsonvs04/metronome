/** \file MenuAbout.java. The "About" activity */

package com.arnaud.metronome;

import android.app.Activity;
import android.os.Bundle;

public class MenuAbout extends Activity
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menuabout);
	}
}
