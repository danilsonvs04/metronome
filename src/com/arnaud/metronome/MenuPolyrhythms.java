/** \file MenuPolyrhythms.java. Polyrhythms Menu */

package com.arnaud.metronome;

import android.app.Activity;
import android.os.Bundle;
import android.content.Context;

import android.view.View;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.EditText;

import android.widget.Toast;
import android.app.AlertDialog;
import android.content.DialogInterface;

import android.content.Intent;//For passing data with Metronome

//import android.util.Log;

/** This is very similar to MenuStructures.java.
* It's a dumbed down version.
* Stack several rhythms that will be played simultaneously.
* The number of possible rhythms to be played depends on the number of available cores from the cpu.
* We can choose specific sound through a minisettings activity, the data is "stored" as a tag in the child(0) of the rhythm, then cast as a View. Hack...
*/

public class MenuPolyrhythms extends Activity
{
	private ContainerPolyrhythms container;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menupolyrhythms);
		
		Intent intent = getIntent();
		container = (ContainerPolyrhythms)intent.getSerializableExtra("containerPolyrhythms");
		setViewFromContainer(container);
	}

	private void setViewFromContainer (ContainerPolyrhythms container)
	{
		/* Prepare UI */
		((LinearLayout) findViewById(R.id.menuPolyrhythmsLinearLayoutRhythms)).removeAllViews();

		for(int i=0;i<container.rhythms.length;i++) {
			RelativeLayout rhythmLayout = buildRhythm();
			((EditText)((LinearLayout)rhythmLayout.getChildAt(0)).getChildAt(1)).setText(String.valueOf(container.rhythms[i].bpm));
			((EditText)((LinearLayout)rhythmLayout.getChildAt(0)).getChildAt(2)).setText(String.valueOf(container.rhythms[i].bar));
			String settings = new String(container.rhythms[i].ticChoice+" "+container.rhythms[i].tocChoice);
			rhythmLayout.getChildAt(0).setTag(settings);
		}
	}

	private boolean setContainerFromView ()
	{
		/* Prepare Data */
		container = new ContainerPolyrhythms();

		LinearLayout rhythmsLayout = (LinearLayout) findViewById(R.id.menuPolyrhythmsLinearLayoutRhythms);
		int rhythmsCount = rhythmsLayout.getChildCount();
		container.rhythms = new Rhythm[rhythmsCount];
		for(int i=0;i<rhythmsCount;i++) {
			RelativeLayout rhythmLayout = (RelativeLayout)rhythmsLayout.getChildAt(i);
			container.rhythms[i] = new Rhythm();

			String bpm = ((EditText)((LinearLayout)rhythmLayout.getChildAt(0)).getChildAt(1)).getText().toString();
			String bar = ((EditText)((LinearLayout)rhythmLayout.getChildAt(0)).getChildAt(2)).getText().toString();
			if (bpm.matches("") || bar.matches("")) {
				Toast.makeText(getApplicationContext(),R.string.menuPolyrhythmsToastRhythmWarning,Toast.LENGTH_SHORT).show();
				return false;
			}
			if (Double.parseDouble(bpm) <= 0) {
				Toast.makeText(getApplicationContext(),R.string.toastBpmCantBeZero,Toast.LENGTH_SHORT).show();
				return false;
			}
			container.rhythms[i].bpm = Double.parseDouble(bpm);
			container.rhythms[i].bar = Double.parseDouble(bar);

			String[] settings = ((View) rhythmLayout.getChildAt(0)).getTag().toString().split(" ");
			container.rhythms[i].ticChoice = Integer.parseInt(settings[0]);
			container.rhythms[i].tocChoice = Integer.parseInt(settings[1]);
		}
		return true;
	}

	/** Append a new rhythmLayout to the PolyrhythmsLayout */
	public RelativeLayout buildRhythm () {
		//On inflate
		final LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout rhythmLayout = (RelativeLayout)inflater.inflate(R.layout.menupolyrhythmsrhythm,null);

		//On l'ajoute et on rempli le textview par rapport à la position dans polyrhythmslayout
		LinearLayout polyrhythmsLayout = (LinearLayout) findViewById(R.id.menuPolyrhythmsLinearLayoutRhythms);
		polyrhythmsLayout.addView(rhythmLayout);

		return rhythmLayout;
	}

	/** UI callback */
	public void onAddRhythm (View v) {
		buildRhythm();
	}

	/** UI callback */
	public void onMiniSettings (View v)
	{
		int i = ((LinearLayout) findViewById(R.id.menuPolyrhythmsLinearLayoutRhythms)).indexOfChild((View) ((View) v.getParent()).getParent());
		String[] settings = ((View) v.getParent()).getTag().toString().split(" ");

		Intent intent = new Intent(MenuPolyrhythms.this, MiniSettings.class);
		intent.putExtra("tic",Integer.parseInt(settings[0]));
		intent.putExtra("toc",Integer.parseInt(settings[1]));
		startActivityForResult(intent,i);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == 1) {
			int tic = data.getIntExtra("tic",-1);
			int toc = data.getIntExtra("toc",-1);

			String settings = new String(tic+" "+toc);
			((RelativeLayout) ((LinearLayout) findViewById(R.id.menuPolyrhythmsLinearLayoutRhythms)).getChildAt(requestCode)).getChildAt(0).setTag(settings);
		}
	}

	/** UI callback */
	public void onDeleteRhythm (View v)
	{
		LinearLayout polyrhythmsLayout = (LinearLayout) findViewById(R.id.menuPolyrhythmsLinearLayoutRhythms);
		polyrhythmsLayout.removeView((View) v.getParent());
	}

	public void onMenuPolyrhythmsOk (View v)
	{
		if (setContainerFromView()) {
			Intent intent = new Intent();
			intent.putExtra("containerPolyrhythms",container);
			setResult(1,intent);
			finish();
		}
	}

	public void onMenuPolyrhythmsCancel (View v)
	{
		setResult(0,null);
		finish();
	}
	
	public void onMenuPolyrhythmsSave (View v)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(MenuPolyrhythms.this);
		builder.setTitle(R.string.dialogSave_title).setMessage(R.string.dialogSave_message);
		builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick (DialogInterface dialog, int id) {
				if (setContainerFromView())
					container.saveToInternal(getApplicationContext());
			}
		});
		builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
			public void onClick (DialogInterface dialog, int id) {
			}
		});

		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	public void onMenuPolyrhythmsLoad (View v)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(MenuPolyrhythms.this);
		builder.setTitle(R.string.dialogLoad_title).setMessage(R.string.dialogLoad_message);
		builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick (DialogInterface dialog, int id) {
				ContainerPolyrhythms instance = new ContainerPolyrhythms();
				container = instance.loadFromInternal(getApplicationContext());
				setViewFromContainer(container);
			}
		});
		builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
			public void onClick (DialogInterface dialog, int id) {
			}
		});

		AlertDialog dialog = builder.create();
		dialog.show();
	}
}
