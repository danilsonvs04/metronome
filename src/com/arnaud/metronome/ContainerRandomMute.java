/** \file ContainerRandomMute.java */

package com.arnaud.metronome;

import android.content.Context;

import java.io.Serializable;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;

/** This is very similar to MenuPolyrhythms or structure.java.
* It's a dumbed down version */

class ContainerRandomMute implements Serializable
{
	private String FILENAME = "randommute";
	//The defaults values are set here because, the container is read first and need to provide default value (like MenuSettings) It's the opposite of poly and struct where the number of elements being 0 nothing is displayed and the default are provided by the EditText definition in layouts.
	int freqMean = 4;
	int freqDev = 2;
	int lengthMean = 4;
	int lengthDev = 2;

	public void saveToInternal (Context context)
	{
		try {//Forcé de faire ça. Java c'est reloud.
			FileOutputStream fos = context.openFileOutput(FILENAME, context.MODE_PRIVATE);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this);
			oos.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public ContainerRandomMute loadFromInternal (Context context)
	{
		ContainerRandomMute container = new ContainerRandomMute();
		
		try {
			FileInputStream fis = context.openFileInput(FILENAME);
			ObjectInputStream ois = new ObjectInputStream(fis);
			container = (ContainerRandomMute) ois.readObject();
			ois.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return container;
	}
}
